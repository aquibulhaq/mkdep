PREFIX=	/usr/local
PROG=	mkdep
MAN_N=	1
MAN=	$(PROG).$(MAN_N)

.PHONY: all install

all: $(PROG)
	chmod a+x $(PROG)

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/share/man/man$(MAN_N)
	cp -f $(PROG) $(DESTDIR)$(PREFIX)/bin
	gzip < $(MAN) > $(DESTDIR)$(PREFIX)/share/man/man$(MAN_N)/$(MAN).gz
