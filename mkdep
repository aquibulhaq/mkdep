#!/bin/sh

aflag=0
DEP=.depend
unset Pval
pflag=0
sflag=0
sval=.o
xflag=0

echo_err()
{
	>&2 echo "$@"
}

usage()
{
	echo_err "usage: mkdep [-apx] [-f depfile] [-s suffix] [-P prefix] file..."
	exit 1
}

while getopts :af:ps:xP: c; do
	case $c in
	# Append to the end of depfile.
	a)
		aflag=1;;
	# Select depfile.
	f)
		DEP="$OPTARG";;
	# Generate dependencies like foo: foo.c
	p)
		pflag=1
		[ $sflag -eq 0 ] && unset sval
		;;
	# Specify a suffix.
	s)
		sflag=1
		sval="$OPTARG"
		;;
	# Generate explicit rules.
	x)
		xflag=1;;
	# Specify a prefix.
	P)
		Pval="$OPTARG";;
	*)
		usage;;
	esac
done
shift `expr $OPTIND - 1`

[ $# -eq 0 ] && usage

TMP=`mktemp mkdep.XXXXXXXXXX`

for file in "$@"; do
	out_file=`echo $file | sed -E "s(.*)\.(c|cpp)${Pval}\1${sval}"`
	$CC -E $CFLAGS -MM -MT "$out_file" "$file" >> $TMP

	[ $xflag -eq 0 ] && continue

	if [ $pflag -ne 0 ]; then
		echo -e '\t$(CC) $(CFLAGS) $(LDFLAGS) -o' "$out_file $file" \
		    >> $TMP
	else
		echo -e '\t$(CC) $(CFLAGS) -c -o' "$out_file $file" >> $TMP
	fi
done

if [ $aflag -ne 0 ]; then
	cat $TMP >> $DEP
	rm -f $TMP
else
	mv $TMP $DEP
fi

exit 0
